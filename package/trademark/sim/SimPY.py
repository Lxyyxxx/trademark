### 1.1 拼音相似度 ###

import pandas as pd
import numpy as np
import itertools
import os

from ..tools.SimTools import SimTools
from ..conf import SIM_PATH

class PrePY:
    """拼音相关预处理"""
    def __init__(self):
        self.group_py_table = pd.read_csv(os.path.join(SIM_PATH, 'files/word_sim_py_cluster_id.csv'), names=['word', 'group']).groupby('word').group.apply(int).to_dict()
        self.py_data = np.load(os.path.join(SIM_PATH, 'files/py.npy'))
        self.first = int(0x4e00)
        
    # 得到【group id】
    def getGroup(self, tmcn):
        return '/'.join([str(self.group_py_table[word]) for word in list(tmcn)])
    
    # 得到【拼音列表】
    def getPinyin(self, tmcn):
        return ["".join(i) for i in itertools.product(*([self.py_data[ord(s)-self.first].split(' ') for s in tmcn]))]

class SimPY:
    """拼音相似度"""
    def __init__(self, threshold=0.895):
        self.prepy = PrePY()
        self.py_threshold = threshold  # 0.90 
    
    # 得到【拼音相似度、是否相似、相似度最高的两个拼音】
    def getSim(self, tm1, tm2):
        tm1pys, tm2pys = self.prepy.getPinyin(tm1), self.prepy.getPinyin(tm2)
        maxDisjw = 0
        res1py, re2py = "", ""
        for tm1py in tm1pys:
            for tm2py in tm2pys:
                disjw = SimTools(tm1py, tm2py).JaroWinkler()
                if disjw > maxDisjw:
                    maxDisjw = disjw
                    res1py, re2py = tm1py, tm2py
        py_sim = maxDisjw
        py_res = (py_sim >= self.py_threshold)
        return float(py_sim), bool(py_res), res1py, re2py

    
if __name__ == "__main__":
    # pre
    prepy = PrePY()
    print(prepy.getGroup('百思')) # '368/29'
    print(prepy.getPinyin('百思')) # ['bai/si/', 'bai/sai/', 'bo/si/', 'bo/sai/', 'mo/si/', 'mo/sai/']
    # sim
    simpy = SimPY()
    print(simpy.getSim('百思', '新安')) # (0.6190476190476191, False, 'bai/si/', 'xin/an/')
