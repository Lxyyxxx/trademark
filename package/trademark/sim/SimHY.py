### 1.3 含义相似度 ###

from googletrans import Translator
import jieba
import httpcore._exceptions
import requests

from ..tools.SimTools import SimTools

class PreHY:
    """含义相关预处理"""
    def __init__(self):
        self.translator = Translator(service_urls=['translate.google.cn'])
    
    # 得到【英文翻译】
    def getEN(self, tmcn):
        try:
            return self.translator.translate(tmcn, src='zh-cn', dest='en').text
        except httpcore._exceptions.ConnectTimeout:
            return None
        except :
            return None
    
    # 根据【英文翻译】得到【英文翻译列表】
    def getList(self, tmen):
        if tmen:
            return tmen.lower().split()
        else:
            return None
    
    # 得到【英文翻译列表】
    def getListEN(self, tmcn):
        return self.getList(self.getEN(tmcn))
    
    # 得到【中文分词】
    def getFenci(self, tmcn):
        return jieba.lcut(tmcn)
    
class SimHY:
    """含义相似度"""
    def __init__(self, threshold=0.295):
        self.prehy = PreHY()
#         self.hy_threshold = 0.495  # 0.50
        self.hy_threshold = threshold  # 0.30
    
    # 得到【含义相似度、是否相似、英文翻译】
    def getSim(self, tm1, tm2):
        tm1fc, tm2fc = self.prehy.getFenci(tm1), self.prehy.getFenci(tm2)
        tm1fy, tm2fy = self.prehy.getListEN(tm1), self.prehy.getListEN(tm2)
        if tm1fy and tm2fy:
            hy_sim = max(SimTools(tm1fc, tm2fc).Bow(), SimTools(tm1fy, tm2fy).Bow())
        else:
            print("只采用分词向量计算相似度")
            hy_sim = SimTools(tm1fc, tm2fc).Bow()
        hy_res = (hy_sim >= self.hy_threshold)
        if tm1fy and tm2fy:
            return float(hy_sim), bool(hy_res), ' '.join(tm1fy), ' '.join(tm2fy)
        else:
            return float(hy_sim), bool(hy_res), None, None

if __name__ == "__main__":
    # pre
    prehy = PreHY()
    print(prehy.getEN('百思')) # 'Best'
    print(prehy.getList(prehy.getEN('百思'))) # ['best']
    print(prehy.getListEN('百思')) # ['best']
    # sim
    simhy = SimHY()
    print(simhy.getSim('百思', '新安')) # (0.0, False, ['best'], ["xin'an"])
