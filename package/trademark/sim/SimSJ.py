### 1.4 视觉相似度 ###

import numpy as np
import os

from ..tools.SimTools import SimTools
from ..conf import SIM_PATH

class PreSJ:
    """视觉相关预处理"""
    def __init__(self):
        self.st16 = np.load(os.path.join(SIM_PATH, 'files/st16bool.npy'))
        self.first = int(0x4e00)
        # 特征笔画卷积核及形状
        self.conv = np.array([
            # ㇀
            [
                [0, 1],
                [1, 0]
            ],
            # ㇔
            [
                [1, 0],
                [0, 1]
            ],
            # 一
            [
                [1, 1],
                [0, 0]
            ],
            # ㇑
            [
                [1, 0],
                [1, 0]
            ]
        ], dtype=np.bool_)
        self.conv_num, self.conv_row, self.conv_col = self.conv.shape
        # 每个卷积核的和
        self.conv_add = [2 for i in range(self.conv_num)]
        # 局部滑动窗口的步长和窗口大小
        self.stride = 4
        self.window_size = self.stride
    
    # 得到【单个字符的点阵图】
    def getBit(self, word):
        return self.st16[ord(word)-self.first]
    
    # 得到【所有字合起来的点阵图】
    def getPic(self, tmcn):
        pic_m_ans = np.empty((16, 16), dtype=np.bool_)
        for i in range(len(tmcn)):
            pic_m = self.getBit(tmcn[i])
            if i:
                pic_m_ans = np.hstack((pic_m_ans, pic_m))
            else:
                pic_m_ans = pic_m.copy()
        return pic_m_ans
    
    # 打印点阵图
    def printPic(self, pic: np.ndarray, foreground: str = '■', background: str = ' '):
        for r in pic:
            for i in r:
                print(foreground, end=' ') if i else print(background, end=' ')
            print()
    
    # 对【局部图片】进行卷积操作提取特征，得到【特征向量】
    def getConv(self, pic):
        row, col = pic.shape
        row, col = row - self.conv_row + 1, col - self.conv_col + 1
        # 特征笔画向量初始化
        res = np.zeros(self.conv_num, dtype=np.int_)
        # 遍历滑动窗口
        for pi in range(row):
            for pj in range(col):
                # cnt += 1
                # 遍历滤波器
                for fn in range(self.conv_num):
                    conv_sum = np.sum(pic[pi:pi+self.conv_row, pj:pj+self.conv_col] & self.conv[fn])
                    res[fn] += (conv_sum//2)
        return res
    
    # ndarray -> 01 string
    def ndarray2bit(self, nd):
        return ''.join(['1' if i else '0' for i in nd.flatten().tolist()])
    
    # 01 string -> ndarray
    def bit2ndarray(self, code):
        nd = np.array([False if i=='0' else True for i in code]).reshape(16, -1)
        return nd
    
    # 01 string
    def getCode(self, tmcn):
        return self.ndarray2bit(self.getPic(tmcn))

class SimSJ:
    """视觉相似度"""
    def __init__(self, threshold=0.895):
        self.presj = PreSJ()
        self.sj_threshold = threshold  # 0.90 
    
    # 得到【视觉相似度、是否相似】
    def getSim(self, tm1, tm2):
        tm1dz, tm2dz = self.presj.getPic(tm1), self.presj.getPic(tm2)
        dif = tm2dz.shape[1] - tm1dz.shape[1]
        # 填补不一样长的部分
        if dif > 0:
            tm1dz = np.hstack((tm1dz, np.zeros((16, dif), np.bool_)))
        elif dif < 0:
            tm2dz = np.hstack((tm2dz, np.zeros((16, -dif), np.bool_)))
        row, col = tm1dz.shape
        # 局部矩阵的个数
        num = row // self.presj.stride * col // self.presj.stride
        # 外圈补 0
        tm1dz = np.pad(tm1dz, ((self.presj.conv_row - 1, self.presj.conv_row - 1), (self.presj.conv_col - 1, self.presj.conv_col - 1)), 'constant', constant_values=(False, False))
        tm2dz = np.pad(tm2dz, ((self.presj.conv_row - 1, self.presj.conv_row - 1), (self.presj.conv_col - 1, self.presj.conv_col - 1)), 'constant', constant_values=(False, False))
        # 初始化相似结果结果为空
        sims = np.empty(num, dtype=np.float_)
        # 遍历每个局部矩阵
        for i in range(0, row, self.presj.stride):
            for j in range(0, col, self.presj.stride):
                # 对每个局部矩阵，计算
                vec1, vec2 = self.presj.getConv(tm1dz[i:i+self.presj.window_size+2, j:j+self.presj.window_size+2]), self.presj.getConv(tm2dz[i:i+self.presj.window_size+2, j:j+self.presj.window_size+2])
                cur = i // self.presj.stride * col // self.presj.stride + j // self.presj.stride
                # 如果有全 0 向量的话，夹角余弦无意义
                sum1, sum2 = sum(vec1), sum(vec2)
                # 有0
                if sum1 == 0 or sum2 == 0:
                    sims[cur] = SimTools(tm2dz[i:i+self.presj.window_size+2, j:j+self.presj.window_size+2], tm1dz[i:i+self.presj.window_size+2, j:j+self.presj.window_size+2]).Xor()
                # 均不为0，余弦相似度
                else:
                    sims[cur] = SimTools(vec1, vec2).Cos()
        sj_sim = np.mean(sims)
        sj_res = (sj_sim >= self.sj_threshold)
        return float(sj_sim), bool(sj_res)

if __name__ == "__main__":
    # pre
    presj = PreSJ()
    presj.printPic(presj.getPic('百思'))
    # sim
    simsj = SimSJ()
    print(simsj.getSim('百思', '新安')) # (0.6339143579193496, False)
