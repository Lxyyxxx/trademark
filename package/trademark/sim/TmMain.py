### 商标合集 ###

# import sys
# sys.path.append('..')
# sys.path.append('../tools')

# 1
from .SimPY import *
from .SimZX import *
from .SimHY import *
from .SimSJ import *
# 2
from .Server import *
from .Place import *
# 4
from .Dirty import *

class TmMain:
    def __init__(self):
        self.simpy = SimPY()
        self.simzx = SimZX()
        self.simhy = SimHY()
        self.simsj = SimSJ()
    
    # tm1: ('tmcn', 'citycode', 'intcls', 'tmdetail')
    # tm2: ('tmcn', 'citycode', 'intcls', 'tmdetail, 'tmapplicant'')
    def getResult(self, tm1, tm2):
        res = {
            "pinyin": {
                "sim": None,
                "res": None,
                "tm1": None,
                "tm2": None
            },
            "zixing": {
                "sim": None,
                "res": None,
                "tm1": None,
                "tm2": None
            },
            "hanyi": {
                "sim": None,
                "res": None,
                "tm1": None,
                "tm2": None
            },
            "shijue": {
                "sim": None,
                "res": None,
            },
            "sim_code": 0,
            "server": None,
            "locate": None,
            "aim": None
        }
        sv_res = {
            "same": {
                "code": None,
                "len": None,
                "uid": None
            },
            "most_diff": {
                "tm1": None,
                "tm2": None,
                "cnt": None,
                "uid": None
            },
            "most_same": {
                "code": None,
                "tm1": None,
                "tm2": None,
                "cnt": None,
                "uid": None,
                "uid": None
            }
        }
        # tm1: (tmcn, citycode, (intcls, tmdetail))
        # 1
        res['pinyin']['sim'],  res['pinyin']['res'], res['pinyin']['tm1'], res['pinyin']['tm2'] = self.simpy.getSim(tm1['tmcn'], tm2['tmcn'])
        res['zixing']['sim'],  res['zixing']['res'], res['zixing']['tm1'], res['zixing']['tm2'] = self.simzx.getSim(tm1['tmcn'], tm2['tmcn'])
        res['hanyi']['sim'],  res['hanyi']['res'], res['hanyi']['tm1'], res['hanyi']['tm2'] = self.simhy.getSim(tm1['tmcn'], tm2['tmcn'])
        res['shijue']['sim'],  res['shijue']['res'] = self.simsj.getSim(tm1['tmcn'], tm2['tmcn'])
        # sim res
        res['sim_code'] |= res['pinyin']['res']        # 0b0001
        res['sim_code'] |= (res['zixing']['res'] << 1) # 0b0010
        res['sim_code'] |= (res['hanyi']['res'] << 2)  # 0b0100
        res['sim_code'] |= (res['shijue']['res'] << 3) # 0b1000
        print(tm2['tmcn'], res['sim_code'])
        # 相似，则继续计算，否则就下一个
        if res['sim_code']:
            # 2
            tmp_same, tmp_diff, tmp_len = 0, 0, 0
            for tm in tm1['info']:
                tm = [*tm.values()][0]
#                 print(tm, tm2)
                sv = Server(tm, tm2)
                # 最大相同服务
                code = sv.getSameServer()
                l = len(code)
                if l > tmp_len:
                    tmp_len = l
                    sv_res['same'] = {
                        "code": code,
                        "len": l,
                        "uid": tm['uid']
                    }
                # 最相关异类服务
                diff = sv.getMostDiffServer()
                if diff and diff[2] > tmp_diff:
                    tmp_diff = diff[2]
                    sv_res['most_diff'] = {
                        "tm1": diff[0],
                        "tm2": diff[1],
                        "cnt": diff[2],
                        "uid": tm['uid']
                    }
                # 最大同类服务细节
                same = sv.getMostSameServer()
                if same and same[3] > tmp_same:
                    tmp_same = same[3]
                    sv_res['most_same'] = {
                        "code": same[0],
                        "tm1": same[1],
                        "tm2": same[2],
                        "cnt": same[3],
                        "uid": tm['uid']
                    }
#                 print(code, diff, same)
            res['server'] = sv_res
    #         sv = Server(tm1, tm2)
    #         res['server']['same'] = sv.getSameServer()
    #         res['server']['most_diff']['tm1'], res['server']['most_diff']['tm2'], res['server']['most_diff']['cnt'] = sv.getMostDiffServer()
    #         res['server']['most_same']['code'], res['server']['most_same']['tm1'], res['server']['most_same']['tm2'], res['server']['most_same']['cnt'] = sv.getMostSameServer()
            plc = Place(tm1, tm2)
            res['locate'] = plc.sameCity() # 同市就算同一个地区
            # 4
            res['aim'] = Dirty(tm2['tmapplicant']).getResult() # tm2 为 被异议人，搜索该公司是否目的不纯
            return res
        else:
            return None

if __name__ == '__main__':
    # es
    import sys
    sys.path.append('..')
    from db_conf import *
    from elasticsearch import Elasticsearch
    es = Elasticsearch(ES_HOST, port=ES_PORT)
    # 测试用例
    qbody = {
        "query": {
            "constant_score" :{
                "filter": {
                    "bool": {
                        "must": [
                            {"exists": {"field": "towncode"}}, # towncode 存在
                            {"regexp": {"tmcn": '[\u4e00-\u9fa5]+'}} # 中文
                        ],
                        "must_not": [
                            {'term': {'tmcn.keyword': '图形'}} # 去掉图形
                        ]
                    }
                }
            }
        },
        "size": 2,
        "_source": ['tmcn', 'citycode', 'intcls', 'tmdetail', 'tmapplicant']
    }
    res = es.search(index=ES_ALL, body=qbody)
    tm1 = res['hits']['hits'][0]['_source']
    tm2 = res['hits']['hits'][1]['_source']
    # run
    import pprint
    pprint.pprint(TmMain().getResult(tm1, tm2))
    # time 5.63
