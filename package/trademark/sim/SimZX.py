### 1.2 字形相似度 ###

import pandas as pd
import numpy as np
import os

from ..tools.SimTools import SimTools
from ..conf import SIM_PATH

class PreZX:
    """字形相关预处理"""
    def __init__(self):
        self.group_zx_table = pd.read_csv(os.path.join(SIM_PATH, 'files/word_sim_cluster.csv'), names=['word', 'group']).groupby('word').group.apply(int).to_dict()
        self.bs_data = np.load(os.path.join(SIM_PATH, 'files/bs.npy'))
        self.first = int(0x4e00)
        
    # 得到【group id】
    def getGroup(self, tmcn):
        return '/'.join([str(self.group_zx_table[word]) for word in list(tmcn)])
    
    # 得到【笔顺编码】
    def getBishun(self, tmcn):
        return ''.join([self.bs_data[ord(s)-self.first] for s in tmcn])

class SimZX:
    """字形相似度"""
    def __init__(self, threshold=0.945):
        self.prezx = PreZX()
        self.zx_threshold = threshold  # 0.95
    
    # 得到【字形相似度、是否相似、笔顺码】
    def getSim(self, tm1, tm2):
        tm1zx, tm2zx = self.prezx.getBishun(tm1), self.prezx.getBishun(tm2)
        zx_sim = SimTools(tm1zx, tm2zx).JaroWinkler()
        zx_res = (zx_sim >= self.zx_threshold)
        return float(zx_sim), bool(zx_res), tm1zx, tm2zx


if __name__ == "__main__":
    # pre
    prezx = PreZX()
    print(prezx.getGroup('百思')) # '1523/4637'
    print(prezx.getBishun('百思')) # '132511/251214544/'
    # sim
    simzx = SimZX()
    print(simzx.getSim('百思', '新安')) # (0.7655462184873949, False, '132511/251214544/', '4143112343312/445531/')
