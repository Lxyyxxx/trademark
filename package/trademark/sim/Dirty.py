### 4 目的不纯 ###

from elasticsearch import Elasticsearch

from ..tools.DictTools import addDict
from ..tools.DateTimeTools import *
from .Server import PreServer
from ..conf import *

class Dirty:
    def __init__(self, tmapplicant):
        self.tmapplicant = tmapplicant
        self.qbody =  {
            "query": {
                "constant_score" :{
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"tmapplicant.keyword": self.tmapplicant}},
                                {"regexp": {"tmcn": '[\u4e00-\u9fa5]+'}} # 中文
                            ],
                            "must_not": [
                                {'term': {'tmcn.keyword': '图形'}} # 去掉图形
                            ]
                        }
                    }
                }
            },
            'size': 10000, # default 10, max 10000
#             "track_total_hits": True,
            '_source': ['tmcn', 'regno', 'intcls', 'tmstatus',  'appdate', 'tmdetail', 'similargroup']
        }
        self.presv = PreServer()
        self.pct_threshold = 0.2
        self.tmcn_num_threshold = 5
    
    def getResult(self):
        # es 搜索该公司申请的所有商标
        es = Elasticsearch(ES_HOST, port=ES_PORT)
        res = es.search(index=ES_ALL, body=self.qbody) # TODO ES_HZ or ES_ALL
        # 将该公司申请的所有商标按年份分组
        tm_years = {}
        for tm in res['hits']['hits']:
            keys = DateTimeTools(tm['_source']['appdate']).getYear()
#             tm['_source']['_id'] = tm['_id'] # 加入_id
            addDict(tm_years, keys, tm['_source'])
        # 判断每个分组中各商标的类别是否相近
        dir_res = []
        for year in tm_years:
            group = {}
#             tmcn_list = set()
            tmcn_list = []
            for i in tm_years[year]:
#                 tmcn_list.add(i['tmcn'])
                tmcn_list.append(i['tmcn'])
                group.update(self.presv.getGroupDict(i))
#             print(group)
            uniq_group = len(list(group.keys()))
            group_num = 0
            for v in list(group.values()):
                group_num += len(v)
            # div 0 err
            if group_num == 0:
                continue
            pct = uniq_group / group_num
            tmcn_num = len(tmcn_list)
#             print(uniq_group, group_num)
#             print(year, tmcn_num, tmcn_list, pct)
#             print('#############')
            # 根据组别数量除总组数量判断集中度，1/5 即为合格，多于的都是目的不纯，且注册的商标要足够多
            if pct > self.pct_threshold and tmcn_num > self.tmcn_num_threshold:
                dir_res.append({
                    'year': year,
                    'cnt': tmcn_num,
                    'tm': '、'.join(tmcn_list)
                })
        return dir_res

if __name__ == "__main__":
    print(Dirty('大幸药品株式会社').getResult())
