### 2.4 同属于一个地区 ###

class Place:
    """地区相关"""
    def __init__(self, tm1js, tm2js):
        self.tm1js = tm1js
        self.tm2js = tm2js
    
    # 同一省
    def sameProvince(self):
        return self.tm1js['provincecode'] == self.tm2js['provincecode']
    
    # 同一市
    def sameCity(self):
        return self.tm1js['citycode'] == self.tm2js['citycode']
    
    # 同一区
    def sameCounty(self):
        return self.tm1js['countycode'] == self.tm2js['countycode']
    
    # 同一镇
    def sameTown(self):
        return self.tm1js['towncode'] == self.tm2js['towncode']

if __name__ == "__main__":
    # es
    import sys
    sys.path.append('../') # 上级引入
    from db_conf import *
    from elasticsearch import Elasticsearch
    es = Elasticsearch(ES_HOST, port=ES_PORT)
    # 测试用例
    qbody = {
        "query": {
            "constant_score" :{
                "filter": {
                    "bool": {
                        "must": [
                            {"exists": {"field": "towncode"}}, # towncode 存在
                            {"regexp": {"tmcn": '[\u4e00-\u9fa5]+'}} # 中文
                        ]
                    }
                }
            }
        },
        "size": 2
    }
    res = es.search(index=ES_HZ, body=qbody)
    tm1 = res['hits']['hits'][0]['_source']
    tm2 = res['hits']['hits'][1]['_source']
    # place
    plc = Place(tm1, tm2)
    print(plc.sameProvince()) # True
    print(plc.sameCity()) # True
    print(plc.sameCounty()) # False
    print(plc.sameTown()) # False
