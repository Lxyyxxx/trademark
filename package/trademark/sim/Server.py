### 2 业务范围 2.1-2.3 ###

import re

from ..tools.Mongo import Mongo
from ..tools.DictTools import addDict
from ..tools.baidu import baidu

class PreServer:
    """服务相关预处理"""
    def __init__(self):
        # 组别对应的中文名 {group:name}
        self.groupNameSet = {}
        m = Mongo('group')
        for i in m.getAll():
            self.groupNameSet[i['group']] = i['gName']
        m.close()
        # 组别包含服务/商品集合 [{'_id','group','content'}]
        self.partSet = []
        m = Mongo('group_item')
        for i in  m.getAll():
            self.partSet.append(i)
        m.close()
    
    # 得到【相关服务字典】
    def getGroupDict(self, tmjs):
        tm_group = {}
        for i in self.partSet:
            # 去除了 非 intcls 开头的
            if not i['group'].startswith(tmjs['intcls']):
                continue
            try:
                for ise in tmjs['tmdetail']:
                    if re.sub('\(\w+\)', '', ise) in i['content']:
                        addDict(tm_group, i['group'], ise)
            except TypeError: # 'NoneType' object is not iterable
                pass
        return tm_group
    
    # 由【相关服务字典】得到【相关服务列表】
    def getKeys(self, tm_group):
        return list(tm_group.keys())
    

class Server:
    def __init__(self, tm1js, tm2js):
        self.presv = PreServer()
        
        self.tm1group, self.tm2group = self.presv.getGroupDict(tm1js), self.presv.getGroupDict(tm2js)
        self.tm1keys, self.tm2keys = self.presv.getKeys(self.tm1group), self.presv.getKeys(self.tm2group)
        
        self.same_keys = list(set(self.tm1keys) & set(self.tm2keys))
        self.diff_keys = list((set(self.tm1keys) | set(self.tm2keys)) - set(self.same_keys))
        
        self.same1group, self.same2group = {}, {}
        for sa in self.same_keys:
            self.same1group[sa] = self.tm1group[sa]
            self.same2group[sa] = self.tm2group[sa]
        
        self.diff1group, self.diff2group = {}, {}
        for diff in self.diff_keys:
            if diff in self.tm1group:
                self.diff1group[diff] = self.tm1group[diff]
            else:
                self.diff2group[diff] = self.tm2group[diff]
        
        self.num_threshold = 800000 # 100000000
    
    # 2.1 服务品种、产品种类相同或类似 / 同类同组（全部列举）
    def getSameServer(self):
        return self.same_keys
                                                                                    
    # 2.2 提供服务性质相同 / 同类不同组
    def getMostDiffServer(self):
        # 在不属于同一组的服务中寻找相关性，判断百度数量
#         cnt = 0
#         objection_serve = None # tm1
#         disputed_serve = None  # tm2
        for diff1 in self.diff1group:
            for diff2 in self.diff2group:
                key1 = '、'.join(self.diff1group[diff1])
                key2 = '、'.join(self.diff2group[diff2])
                keyword = key1 +  ' ' +key2
                num = baidu(keyword).GetNumber()
                if num > self.num_threshold:
#                     print(key1, key2, num)
#                     print(num, keyword)
                    # 直接返回
                    return key1, key2, num
#                     objection_serve, disputed_serve = key1, key2
#                     cnt = num
#         # 保留相关性最强的说明
        return None, None, 0
    
    # 2.3 相同的受众群体和销售渠道 / 同类同组（最多类别）
    def getMostSameServer(self):
        # 判断服务是否有相等的  
        cnt = 0
        if self.same_keys:
            for sa in self.same_keys:
#                 l = len(self.same1group[sa])
                l = len(set([self.same1group[sa], self.same2group[sa]]))
                if l >= cnt:
#                     print(sa, l)
                    fina_sa = sa
                    cnt = l
            # 只说明最多类别的那一个
#             return fina_sa, self.presv.groupNameSet[fina_sa], '、'.join(set(self.same1group[fina_sa])), cnt 
            return fina_sa, list(set(self.same1group[fina_sa])), list(set(self.same2group[fina_sa])), cnt 

        
if __name__ == "__main__":
    # es
    import sys
    sys.path.append('../') # 上级引入
    from db_conf import *
    from elasticsearch import Elasticsearch
    es = Elasticsearch(ES_HOST, port=ES_PORT)
    # 测试用例
    qbody = {
        "query": {
            "constant_score" :{
                "filter": {
                    "bool": {
                        "must": [
                            {"exists": {"field": "towncode"}}, # towncode 存在
                            {"regexp": {"tmcn": '[\u4e00-\u9fa5]+'}} # 中文
                        ]
                    }
                }
            }
        },
        "size": 2
    }
    res = es.search(index=ES_ALL, body=qbody)
    tm1 = res['hits']['hits'][0]['_source']
    tm2 = res['hits']['hits'][1]['_source']
    # pre
    presv = PreServer()
    print(presv.getGroupDict(tm1)) # {'0102': ['盐类(化学制剂)'], '0105': ['植物用微量元素制剂'], '0106': ['化学试纸', '生物化学催化剂'], '0109': ['盐类(化学制剂)'], '0113': ['醋化用细菌制剂']}
    print(presv.getKeys(presv.getGroupDict(tm1))) # ['0102', '0105', '0106', '0109', '0113']
    # sim
    sv = Server(tm1, tm2)
    print(sv.getSameServer()) # ['0113', '0105', '0106', '0102']
    print(sv.getMostDiffServer()) # ('盐类(化学制剂)', '工业用胶', 6560000)
    print(sv.getMostSameServer()) # ('0106', '化学试剂', '生物化学催化剂、化学试纸', 2)
