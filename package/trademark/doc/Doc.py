### 异议文档生成相关 ###

from ..tools.ChineseNumber import *
# from ..sim.TmMain import *
from ..tools.DateTimeTools import *

class DocTemplate:
    def __init__(self, tm1, tm2, d1=None, d4=None):
        self.tm1 = tm1 # ('tmapplicant', 'tmcn_list', 'tmagent')
        self.tm2 = tm2 # ('tmapplicant', 'tmcn', 'intcls', 'regno', 'tmdetail')
#         self.tmfc = TmMain()
        self.d1 = d1
        self.d4 = d4
    
    def getLaw(self, result):
        law = []
        # 相似 + 恶意
        if result['sim_code']:
            law += ['第 30 条', '第 32 条', '第 45 条']
        # 地域
        if result['locate']:
            law += ['第 7 条', '第 44 条']
        law = sorted(law, key=lambda l: int(l[2:-2]))
        return '、'.join(law)
    
    def generate(self):
#         res = self.tmfc.getResult(self.tm1, self.tm2)
        res = self.tm2['res']
        law = self.getLaw(res)
        title = '商 标 异 议\n'
        content = title + BeginTemplate(self.tm1, self.tm2).generate(res, law) + MidTemplate(self.tm1, self.tm2).generate(res, self.d1, self.d4) + EndTemplate(self.tm1).generate(res, law)
        return content
    
    def to_txt(self):
        filename = '%s_%s异议文档.txt' % (self.tm1['tmcn'], self.tm2['tmcn'])
        with open(filename, 'w') as f:
            f.write(self.generate())
        return filename

    
class BeginTemplate:
    """开头的声明模板，不含标题序号"""
    def __init__(self, tm1, tm2):
        self.cn = ChineseNumber()
        self.tm1 = tm1 # ('tmapplicant', 'tmcn_list', 'tmagent')
        self.tm2 = tm2 # ('tmapplicant', 'tmcn', 'intcls', 'regno', 'tmdetail')
    
    # 本异议书有关概念
    def p1(self):
        content = '\n本异议书有关概念：\n'
        content += '异议人： %s\n' % self.tm1['tmapplicant']
        content += '被异议人： %s\n' % self.tm2['tmapplicant']
        content += '被异议商标： %s\n' % self.tm2['tmcn']
        return content
    
    # 异议人在先权利
    def p2(self):
        content = '\n异议人在先权利\n'
        cnt = 0
        for i in self.tm1['info']:
            cnt += 1
            i = [*i.values()][0]
            content += '引证商标%s：%s（第 %s 类，注册号：%s）\n' % (self.cn.num2cn(cnt), self.tm1['tmcn'], i['intcls'], i['regno'])
        return content
    
    # 主张
    def p3(self):
        content = '\n国家知识产权局商标局：\n'
        if self.tm1['tmagent']: # 有代理人是委托主张
            content += '根据《中华人民共和国商标法》（以下简称：商标法）第三十三条的规定，%s（以下简称“异议人”）现委托%s，向贵局提交对%s（以下简称“被异议人”）在第%s类“%s”服务上申请注册并已初步审定的第%s号%s（以下简称“被异议商标”）商标提出异议申请，请求商标局撤回对被异议商标的初步审定并不予注册。\n' % (self.tm1['tmapplicant'], self.tm1['tmagent'], self.tm2['tmapplicant'], self.tm2['intcls'], '、'.join(self.tm2['tmdetail']), self.tm2['regno'], self.tm2['tmcn'])
        else: # 无代理人是自我主张
            content += '我公司，%s（以下简称“异议人”），已经对初审公告的第%s号商标%s（以下简称“被异议商标”）提出异议。此被异议商标的申请人为%s（以下简称“被异议人”），指定第%s类，范围包括“%s”等。\n' % (self.tm1['tmapplicant'], self.tm2['regno'], self.tm2['tmcn'], self.tm2['tmapplicant'], self.tm2['intcls'], '、'.join(self.tm2['tmdetail']))
        return content
    
    # 异议人阐述事实及理由如下
    def p4(self, result, law):
        content = '\n异议人阐述事实及理由如下：\n异议人认为：'
        # 根据相似查询判断，自动编号
        cnt = 0
        # 相似 + 恶意
        if result['sim_code']:
            cnt += 1
            content += '%s，被异议商标与异议人注册使用在先并已经具有较高知名度、影响力的商标和商号“%s”相似，' % (self.cn.di(cnt), self.tm1['tmcn'])
            # 服务
            if result['server']['most_diff']:
                content += '指定的商品或服务存在密切关联性，'
            content += '且“%s”三个字作文商标独创性较强被异议人的注册明显具有抢注的恶意。' % self.tm1['tmcn']
        # 地域
        if result['locate']:
            cnt += 1
            content += '%s，引证商标“%s”在被异议商标申请日之前已经持续使用多年，已经为相关公众所熟知，被异议人与异议人同在相同地域，不会不知道%s的存在，被异议人的注册明显违背诚实信用原则，具有不正当性。' % (self.cn.di(cnt), self.tm1['tmcn'], self.tm1['tmcn'])
        if law:
            content += '依据《商标法》%s之规定，' % law
        content += '请求贵局裁定异议理由成立被异议商标不予注册。异议人的具体事实和理由如下：\n'
        return content
    
    def generate(self, res, law):
        return self.p1() + self.p2() + self.p3() + self.p4(res, law)
            
class MidTemplate:
    """中间的理由模板，含标题序号"""
    def __init__(self, tm1, tm2):
        self.cn = ChineseNumber()
        self.tm1 = tm1 # ('tmapplicant', 'tmcn_list', 'tmagent')
        self.tm2 = tm2 # ('tmapplicant', 'tmcn', 'intcls', 'regno', 'tmdetail')
        self.cnt = 0
        self.num2cn = ['', '首先', '其次', '第三', '第四', '第五']
    
    # 品牌介绍，用户手动输入
    def p1(self, doc=None):
        content = ''
        if doc:
            self.cnt += 1
            content += '\n%s、%s品牌介绍\n%s\n' % (self.cn.num2cn(self.cnt), self.tm1['tmcn'], doc)
        return content
    
    # 商标近似 + 业务范围密切相关
    def p2(self, result):
        content = ''
        if result['sim_code'] or result['server']['most_diff'] or result['server']['most_same']:
            self.cnt += 1
            title = '\n%s、' % self.cn.num2cn(self.cnt)
            cnt = 0
            # 商标相似
            if result['sim_code']:
                cnt += 1
                title += '被异议商标与引证商标在商标本身方面近似，'
                content += '%s、被异议商标与引证商标在商标本身方面构成近似\n商标近似，是指商标文字的字形、读音、含义或者图形的构图及颜色，或者其各要素组合后的整体结构相似，或者其立体形状、颜色组合近似，易使相关公众对商品的来源产生误认或者认为其来源与注册商标的商品有特定的联系。\n被异议商标与引证商标很明显' % cnt
                sim = []
                # 拼音
                if result['sim_code'] & 0b0001:
                    sim += ['呼叫']
                # 字形
                if result['sim_code'] & 0b0010:
                    sim += ['文字构成']
                # 含义
                if result['sim_code'] & 0b0100:
                    sim += ['含义']
                # 视觉
                if result['sim_code'] & 0b1000:
                    sim += ['视觉']
                content += '%s相似。被异议人可谓是赤裸裸的抄袭挪用。\n' % '、'.join(sim)
            # 业务范围密切相关
            if result['server']['most_diff']['uid'] or result['server']['most_same']['uid']:
                cnt += 1
                title += '指定商品在实践中存在密切关联，'
                content += '%s、争议商标与引证商标指定商品在实践中存在密切关联\n类似商品，是指在功能、用途、生产部门、销售渠道、消费群体等方面相同，或者相关公众一般认为其存在特定联系、容易造成混淆的商品。在商标授权确权判定中，进行商标法意义上商品类似的判断，并非对相关商品作物理属性的比较，而主要考虑商标能否共存或者决定商标保护范围的大小。\n' % cnt
                content += '本案中，被异议商标核准项目“%s”等。\n' % '、'.join(self.tm2['tmdetail'])
                for i, tm in enumerate(self.tm1['info']):
                    tm = [*tm.values()][0]
                    content += '引证商标 %s 指定使用产品“%s”等。\n' % (str(i+1), '、'.join(tm['tmdetail']))
                if result['server']['most_same']['uid']:
                    content += '因此被异议商标核准服务与引标商标使用产品在%s等方面存在完全相同关系。\n' % '、'.join(set([result['server']['most_same']['tm1'], result['server']['most_same']['tm2']]))
                else:
                    content += '因此被异议商标核准服务与引标商标使用产品在%s等方面存在相似关系。\n' % '、'.join(set([result['server']['most_diff']['tm1'], result['server']['most_diff']['tm2']]))
            title += '二者并存容易导致相关公众误认为系列商标，容易混淆、误认，损害申请人和广大消费者的合法权益。被异议商标的注册违反《商标法》第三十条、第三十二条之规定，应当予以驳回。\n'
            content = title + content
        return content
        
    
    # 恶意性
    def p3(self):
        self.cnt += 1
        content = '\n%s、被异议商标侵犯异议人商号权。\n' % self.cn.num2cn(self.cnt)
        tmdetail = []
        # tm1 所有的 detail 合并
        for i in self.tm1['info']:
            i = [*i.values()][0]
            tmdetail += i['tmdetail']
        # 知名度 + 独创性 + 服务
        content += '首先，“%s”作为商号经过申请人长期使用和宣传，在%s领域已经具有较高的知名度。\n其次，“%s”本身显著性、独创性较强，并非是普通的常用词，很难出现雷同情况。\n第三，被异议商标核准使用服务与申请人经营产品项目构成密切关联近似，如果出现在市场上，很容易导致相关公众产生混淆。\n综上几点，被异议商标的注册构成侵犯申请人商号权的情形。\n' % (self.tm1['tmcn'], '、'.join(set(tmdetail)), self.tm1['tmcn'])
        return content
    
    # 较高的知名度和影响力，用户手动输入
    def p4(self, doc=None):
        content = ''
        if doc:
            self.cnt += 1
            content = '\n%s、在被异议商标申请注册（%s）之前，“%s”商标和商号经异议人的使用，已经在相关公众当中积累了较高的知名度和影响力，与异议人之间建立了唯一对应关系。\n%s\n' % (self.cn.num2cn(self.cnt), DateTimeTools(self.tm2['appdate']).getCN(), self.tm1['tmcn'], doc)
        return content
    
    # 目的不纯
    def p5(self, result):
        content = ''
        # 同地域 + 抢占 + 知名度
        if result['locate'] or result['aim'] or result['server']['most_diff'] or result['server']['most_same']:
            self.cnt += 1
            cnt = 0
            content += '\n%s、被异议人违背了诚实信用原则，扰乱商标注册秩序，其恶意的注册和使用极易误导和欺骗消费者，必然在市场上造成不良影响，依据《商标法》第七条、第四十四条的规定，被异议商标应不予核准注册。\n' % self.cn.num2cn(self.cnt)
            # 地域
            if result['locate']:
                cnt += 1
                content += '%s，被异议人与异议人同属一个地域，在异议人多年使用和宣传的情况下，被异议人对异议人的“%s”商号及商标不会不知道，足见其故意模仿抄袭商誉的恶意。\n' % (self.num2cn[cnt], self.tm1['tmcn'])
            # 抢占
            if result['aim']:
                cnt += 1
                content += '%s，被异议人具有很明显的抢注他人商标的不正当性，经过调查，发现，其' % self.num2cn[cnt]
                tms = []
                for i in result['aim']:
                    tms += ['在 %s 年申请的商标共有 %s 个商标，其中包含%s' % (str(i['year']), str(i['cnt']), i['tm'])]
                content += '%s。这些商标均为他人具有较高知名度的商标，由此可知被异议人的恶意注册行径。换句话说，被异议人是专业从事商标抢注的公司。抢注他人商标，获得不当的利益，是被异议人申请注册商标的根本目的。如果说被申请人注册一个知名商标是偶然，但是在某个时期内申请多个商标，且上述商标均为他人具有较高知名度的商标，且注册类别又都与这些品牌的商品为同一类别，被申请人申请这些商标的就不是偶然了，显然恶意抢注。不难看出，被申请人就是想用这些商标获取利益。\n' % '；'.join(tms)
            # 服务
            if result['server']['most_diff'] or result['server']['most_same']:
                cnt += 1
                content += '%s，如果被异议商标被核准注册，由于异议人在长期使用过程中，已使其“%s”商号和商标在行业中具有极高的知名度和影响力，消费者只是基于一般常识和对异议人服务的信任，误认为被异议商标与异议人存在一定联系，选择了实际上与异议人毫无关联的被异议人的产品与服务，最终将损害广大消费者的合法权益。倘若被异议人产品或服务的质量不能保证，势必会造成消费者对异议人“%s”产生误会，迁怒于异议人，使异议人在名誉上、经济上受到多重损失。\n' % (self.num2cn[cnt], self.tm1['tmcn'], self.tm1['tmcn'])
        return content
    
    def generate(self, res, d1=None, d4=None):
        return self.p1(d1) + self.p2(res) + self.p3() + self.p4(d4) + self.p5(res)


class EndTemplate:
    """结尾的总结模板，不含标题序号"""
    def __init__(self, tm):
        self.tm = tm # ('tmapplicant', 'tmcn_list', 'tmagent')

    # 总结
    def p1(self, res, law):
        content = '\n结论与请求：\n综上所述，被异议人的申请明显具有不正当性，构成对品牌商标和商号的侵权行为，'
        if law:
            content += '被异议商标的注册违反了《商标法》%s之规定，' % law
        content += '依法应当不予注册。异议人恳请贵局查明事实，根据我国相关法律法规的规定，依法裁定异议理由成立，驳回被异议商标的注册申请。\n'
        return content
    
    # 盖章
    def p2(self):
        return '此致 国家工商行政管理总局商标局\n异议人：%s\n代理人：%s\n%s\n' % (self.tm['tmapplicant'], self.tm['tmagent'], Now().getCN())
    
    def generate(self, res, law):
        return self.p1(res, law) + self.p2()
        