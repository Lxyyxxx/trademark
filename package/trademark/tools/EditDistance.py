### Jaro & Jaro winkler & Lavenshtein similarity ###

def calJaro(len_s1: int, len_s2: int, m: int, t: int) -> double:
    if not len_s1 or not len_s2:
        return 0.0
    return (m / len_s1 + m / len_s2 + (m - t) / m) / 3 if m else 0.0

def calJaroWinkler(sim_j, l, p=1):
    return simj + l * p * (1 - sim_j)

def calLev(a, b):
    len_a, len_b = len(a), len(b)
    if not len_b:
        return len_a
    if not len_a:
        return len_b
    if a[0] == b[0]:
        return calLev(a[1:], b[1:])
    return 1 + min(calLev(a[1:], b), calLev(a, b[1:]), calLev(a[1:], b[1:]))

