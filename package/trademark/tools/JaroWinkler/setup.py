# -*- coding: utf-8 -*-
#
# Created by Xinyu on 2019/8/14 22:46
#

from distutils.core import setup,Extension

jarowinkler_module = Extension('_JaroWinkler', sources=['main_wrap.cxx', 'main.cpp'],)


setup(name='JaroWinkler',
      version='1.0',
      author='Xinyu',
      ext_modules=[jarowinkler_module],
      py_modules=['JaroWinkler'],
)
