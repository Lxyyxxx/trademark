# Jaro-Winkler
采用C++編寫，用swig封裝成Python接口

## 安裝方式
``` bash
python3 setup.py build_ext --inplace
```
提示`No module named 'distutils.core'`，先安装pip  
``` bash
apt install python3-pip
```
生成的build文件夹可删除  
可以仅保留`.pyd`(windows)或`.so`(linux)和`JaroWinkler.py`文件使用

## 使用方式
``` python
import JaroWinkler
JaroWinkler.JaroWinklerSimilarity(str1,str2)
```
