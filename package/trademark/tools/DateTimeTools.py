### 时间操作相关 ###

import datetime

class DateTimeTools:
    def __init__(self, dt_str, form='%Y-%m-%d'):
        self.dt_str = dt_str
        self.form = form
        self.dt = datetime.datetime.strptime(self.dt_str, self.form)
        
    def getYear(self) -> int:
        return self.dt.year
    
    def getMonth(self) -> int:
        return self.dt.month
    
    def getDay(self) -> int:
        return self.dt.day
    
    def getCN(self):
        return '%s 年 %s 月 %s 日' % (str(self.dt.year), str(self.dt.month), str(self.dt.day))

class Now:
    def __init__(self):
        self.dt = datetime.datetime.now()
    
    def getCN(self):
        return '%s 年 %s 月 %s 日' % (str(self.dt.year), str(self.dt.month), str(self.dt.day))
