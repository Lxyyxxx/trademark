### 获取百度引擎返回结果数量 ###

import requests
from bs4 import BeautifulSoup
import json
import re

class baidu(object):
    def __init__(self, keyword):
        self.keyword = keyword
  
    def GetNumber(self):
        base_url = 'https://www.baidu.com/s?&tn=baidu'
        params = { 
            'wd': self.keyword,
            'ie': 'UTF-8',
        }
        headers =  {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36',
            'Host': 'www.baidu.com',
            'is_referer': 'https://www.baidu.com/',
            'Referer': 'https://www.baidu.com/',
            'is_xhr': '1',
            'X-Requested-With': 'XMLHttpRequest',
            'Sec-Fetch-Site': 'same-origin',
            'Connection': 'keep-alive',
            'Sec-Fetch-Mode': 'cors',
            'Cookie': 'BIDUPSID=27D99AF62134FE0067A2718FDE54B05C; PSTM=1555372647; __cfduid=d22d2a985952608b579d809c4b1a6c2281557475857; BD_UPN=12314753; BAIDUID=EBFF5B3E3E9A96BA358C924A56ED54AA:FG=1; BDSFRCVID=YJDOJeC62l4f3M3wnWEREHtYokTMD85TH6aoUsUYqAtL0oosbnATEG0P_U8g0Kubd2FpogKK3mOTHREHc2L27PZQ8vZk8_45l_0Jtf8g0M5; H_BDCLCKID_SF=JJkO_D_atKvjDbTnMITHh-F-5fIX5-RLfaPqVPOF5lOTJh0RM650yR-p3l38WPrNMHOqQ-cXQRQ1ehbyqjbke6oMKMQXetjK2CntsJOOaCvOjCbRy4oTLnk1DP8LBT5f-6r8BM0bMRk5Dp3Xh6j1hMK7DqLeBjIqJb-qoDKbfbOSjJOcq4QSMJF0hmT22-us0jnlQhcH0hOWsIOFD55K5T-L5t53Wp3X-b7eXtTG5qnfsMPzDUC0DjPQ-fIX5to05TIX3b7EfbrDs4O_bfbT2MbyQ2c7tJb0B4jXWhj2Jh3GsUTkjx7SQl3LKHbZqjDJJJAjoK_Xf-3bfTrxK4njbJtyhUIX5-RLfangah7F5l8-hR6mhMni0R-0-x5x-hj-K-jKBUOGtxbxOKQphpnDW-uFWJrm5Rb7BecuKlTN3KJmObL9bT3v5tDIeecu2-biWb7L2MbdLDnP_IoG2Mn8M4bb3qOpBtQmJeTxoUJ25DnJhhCGe6LbejOWjHDsbbcE--oDQ6rJabC3fJ-xKU6qLUtbQNbRXtv2bbn7-h51bpnajCTE-R5K5T-7BPrHJMIEtR-JoDDMJDL3qPTuKITaKDCShUFsJPJtB2Q-5KL-fCjEEqr45TO1L4kW0xol5tLqQHbGBfbdJJjzDCLGbPbJKb0yyxJyJx_La2TxoUJh5DnJhhvG-4PKjtCebPRiJ-r9QgbLKpQ7tt5W8ncFbT7l5hKpbt-q0x-jLTnhVn0MBCK0hCLGj5Abj53M5pJfeJJK25TH3RcMHJOoDjrnh6RZyx0gyxomtjjQJIFe2CO5af3pVl5S5hroMp0IQaAtLUkqKCOPQM5w-ljM8KJvhnbDhx_7QttjQpRPfIkja-5t5IoPqb7TyU45hf47yb54BPtHBnufbUclfPQAjIn3j5oSBT8p5MDOK5OuJRQ2QJ8BtK-KhIOP; sugstore=1; delPer=0; BD_CK_SAM=1; PSINO=7; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; BDRCVFR[M7pOaqtZgJR]=mk3SLVN4HKm; H_PS_PSSID=1444_21107_26350_30501; BD_HOME=0; BDSVRTM=2; H_PS_645EC=91b6zB8gww2qV1ra0iBlQ5Geu1eShei6NaUDzNX2sK%2Bk%2BCsysh8bOw4SXJI'
        }
        # cookies=cookieDict
        content = requests.get(base_url, params=params, headers=headers)
        content.encoding = 'uft-8'
        try:
            span_content = BeautifulSoup(content.text, 'html.parser').find(class_="nums_text").get_text()
            nums = re.findall('\d+', span_content)
            num = int(''.join(nums))
            return num
        except AttributeError:
#             print(self.keyword, 'get number error!')
            return 0

if __name__ == '__main__':
    print(baidu(keyword='123456').GetNumber())
