### 计算二者相似度的工具合集 ###

# import gensim
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from .JaroWinkler.JaroWinkler import JaroWinklerSimilarity

class SimTools:
    def __init__(self, a1, a2):
        self.a1 = a1
        self.a2 = a2
    
    # 字符串类：JaroWinkler
    def JaroWinkler(self):
        return JaroWinklerSimilarity(self.a1, self.a2)
    
    # 汉字列表转词向量
    def __getVec(self, a, dic):
        res = [0 for i in dic]
        for i in a:
            for j in range(len(dic)):
                if i == dic[j]:
                    res[j] += 1
                    break
        return res
    
    # 列表类：词向量
    def Bow(self):
#         dictionary = gensim.corpora.Dictionary([self.a1])
#         corpus = [dictionary.doc2bow(text) for text in [self.a1]]
#         index_sim = gensim.similarities.Similarity(None, corpus, len(dictionary.keys()))
#         vec = dictionary.doc2bow(self.a2)
#         sims = index_sim[vec]
#         return sims[0]
        dic = list(set(self.a1).union(set(self.a2)))
        a1_vec, a2_vec = self.__getVec(self.a1, dic), self.__getVec(self.a2, dic)
        a1_vec, a2_vec = np.array(a1_vec).reshape(1, -1), np.array(a2_vec).reshape(1, -1)
        return cosine_similarity(a1_vec, a2_vec)[0][0]
    
    # 向量类：余弦相似度
    def Cos(self):
        self.a1, self.a2 = self.a1.reshape(1, -1), self.a2.reshape(1, -1)
        return cosine_similarity(self.a1, self.a2)
    
    # 矩阵类：异或 / 相同点
    def Xor(self):
        return 1 - np.mean(self.a1 ^ self.a2)

if __name__ == "__main__":
    print(SimTools('bai/si/', 'xin/an/').JaroWinkler())
