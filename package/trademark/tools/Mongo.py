### MongoDB 相关 ###

import pymongo

class Mongo:
    def __init__(self, col):
        self.client = pymongo.MongoClient("mongodb://shangbiaoroot:China%402020!@10.0.0.9/shangbiao?authSource=admin")
        self.db = self.client["shangbiao"]
        self.col = self.db[col]
    
    # 查找所有
    def getAll(self):
        # big data process use no_cursor_timeout = True
        return self.col.find()
    
    # 添加一条
    # item = {'_id': i['_id']}
    def insertOne(self, item):
        if '_id' in item:
            del item['_id']
        self.col.insert_one(item)
    
    # 更新一条
    # condition = {'_id': i['_id']}
    # update = {'zwxx_split': lists}
    def updateOne(self, condition, update):
        try:
            self.col.update_one(condition, {'$set': update})
            print('update success')
        except Exception as e:
            print(e, update)
    
    # 清空所有内容
    def remove(self):
        self.col.remove()
        
    # 删库
    def drop(self):
        self.col.drop()
    
    # 关闭连接
    def close(self):
        self.client.close()

if __name__=="__main__":
    m = Mongo('group')
    for i in  m.getAll():
        print(i)
    m.close()
