### 阿拉伯数字和中文数字 ###

class ChineseNumber:
    def __init__(self):
        self.cn = '〇一二三四五六七八九十'
    
    def ge(self, num):
        return self.cn[num]
    
    def shi(self, num):
        res = ''
        if num > 1:
            res += self.cn[num]
        if num:
            res += '十'
        return res
    
    # 先写一个 0-99 的
    def num2cn(self, num):
        return self.shi(num // 10) + self.ge(num % 10)
    
    def di(self, num):
        return '第%s' % self.num2cn(num) 
