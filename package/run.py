### eve run ###
# %time !curl -d '{"tmcn": "新安", "tmapplicant": "浙江新安化工集团股份有限公司"}' -H 'Content-Type: application/json' http://127.0.0.1:6666/trademarks
# {"_updated": "Wed, 20 Jan 2021 15:09:10 GMT", "_created": "Wed, 20 Jan 2021 15:09:10 GMT", "_etag": "aed7ce5e262102311132eefc65eb4f68966c1405", "_id": "6008479646a2985d568ebcb6", "_links": {"self": {"title": "trademark", "href": "trademarks/6008479646a2985d568ebcb6"}}, "_status": "OK"}
"""
tm2 多进程查询
CPU times: user 7.37 s, sys: 2.71 s, total: 10.1 s
Wall time: 12min 16s
"""
"""
baidu 查询一旦符合条件就返回，而非全部的最大值
CPU times: user 8.55 ms, sys: 0 ns, total: 8.55 ms
Wall time: 2min 17s
"""

from eve import Eve
from elasticsearch import Elasticsearch
from multiprocessing import Pool, Lock, Manager
import pprint

from trademark.conf import *
from trademark.tools.Mongo import *
from trademark.sim.TmMain import *

tm_main = TmMain()
lock = Lock()

def cal(hit, tm1, tm2):
    print('%s is running' % hit['_source']['tmcn'])
    doc = hit['_source']
    # 对每个 doc 计算结果
    res = tm_main.getResult(tm1, hit['_source'])
#     print('doc.res:', res)
    if res:
        doc['res'] = res
#         tm2.append(doc)
        lock.acquire()
        try:
            tm2.append(doc)
#             print(tm2)
        finally:
            lock.release()
#         print('cal tm2:', tm2)
    print('%s is finished' % hit['_source']['tmcn'])    

# event
# https://docs.python-eve.org/en/stable/features.html#event-hooks
# 查找 tm1 相关信息 + 检索相似的 tm2 + 计算相似结果 + 存入 MongoDB
def run(items):
#     print('insert data: ', items) # insert data:  [{'tmcn': '新安', 'tmapplicant': '浙江新安化工集团股份有限公司', '_updated': datetime.datetime(2021, 1, 18, 15, 54, 11), '_created': datetime.datetime(2021, 1, 18, 15, 54, 11), '_etag': 'd51c01023508e755175384b2e0055d7f23401cfd', '_id': ObjectId('6005af2369a0543f12c8e3ac'), '_links': {'self': {'title': 'trademark', 'href': 'trademarks/6005af2369a0543f12c8e3ac'}}, '_status': 'OK'}]
    item = items[0]
    print('searching tm1')
    _id = item['_id']
    tmcn = item['tmcn']
    tmapplicant = item['tmapplicant']
    # 查找 tm1 相关信息
    qbody = {
        "query": {
            "constant_score" :{
                "filter": {
                    "bool": {
                        "must": [
                            {"term": {"tmcn.keyword": tmcn}}, # tmcn
                            {"term": {"tmapplicant.keyword": tmapplicant}}, # tmapplicant
                            {"exists": {"field": "py_group_id"}},
                            {"exists": {"field": "bs_group_id"}},
                            {"term": {"tmstatus": '商标已注册'}} # 有效商标
                        ]
                    }
                }
            }
        },
        "size": 10000,
        "_source": ['citycode', 'intcls', 'tmdetail', 'tmagent', 'regno', 'appdate', 'py_group_id', 'bs_group_id', 'uid']
    }
    res = es.search(index=ES_ALL, body=qbody)
#     # 第 1 个作为主引证，所有塞到 tmcn_list 中
#     tm1 = res['hits']['hits'][0]['_source']
#     tmcn_list = []
#     for i in res['hits']['hits']:
#         tmcn_list.append(i['_source'])
#     tm1['tmcn_list'] = tmcn_list
#     pprint.pprint(tm1)
    tm1 = {
        'tmcn': tmcn,
        'tmapplicant': tmapplicant,
        'tmagent': res['hits']['hits'][0]['_source']['tmagent'],
        'citycode': res['hits']['hits'][0]['_source']['citycode'],
        'info': []
    }
    py_group_id = res['hits']['hits'][0]['_source']['py_group_id']
    bs_group_id = res['hits']['hits'][0]['_source']['bs_group_id']
    for i in res['hits']['hits']:
        doc = i['_source']
        del doc['citycode']
        del doc['py_group_id']
        del doc['bs_group_id']
        del doc['tmagent']
        uid = doc['uid'] # 索引
        tm1['info'].append({uid: doc})
    # 搜索 被异议商标
    print('searching tm2')
    print(_id, tmcn, tmapplicant, bs_group_id, py_group_id)
    qbody = {
        "query": {
            "constant_score" :{
                "filter": {
                    "bool": {
                        "should": [
                            {"term": {"py_group_id.keyword": py_group_id}}, # 拼音相似可能
                            {"term": {"bs_group_id.keyword": bs_group_id}} # 字形相似可能
                        ],
                        "must_not": [
                            {"term": {"tmapplicant.keyword": tmapplicant}}, # 是其他人的商标
                            {"term": {"tmstatus": '商标无效'}} # 无效的就没有必要生成异议文档了
                        ]
                    }
                }
            }
        },
        "size": 10,
        "_source": ['uid', 'tmcn', 'tmapplicant', 'citycode', 'intcls', 'tmdetail', 'tmagent', 'regno', 'appdate', 'tmstatus', 'py_group_id', 'bs_group_id']
    }
    res = es.search(index=ES_ALL, body=qbody)
    print('calculating')
    p = Pool(10)
    # 可共享的 list（进程不共享内存空间，不能直接用 list ）
    tm2 = Manager().list()
    for hit in res['hits']['hits']:
        p.apply_async(cal, args=(hit, tm1, tm2, ))
#         print(r.get())
#     print('Waiting for all subprocesses done...')
    p.close()
    p.join()
#     print('All subprocesses done.')
#     pprint.pprint(tm1)
    tm2 = list(tm2)
#     print('tm2:')
#     pprint.pprint(tm2)
    # 写入 MongoDB 结果
    print('saving')
#     items[0].update({'tm1': tm1, 'tm2': tm2})
    Mongo('trademarks').updateOne({'_id': _id}, {'tm1': tm1, 'tm2': tm2})
#     return _id
    
def finish(updates, original):
    print('update over:', updates, original)

es = Elasticsearch(ES_HOST, port=ES_PORT)

app = Eve()
app.on_inserted_trademarks += run
app.on_updated_trademarks += finish

if __name__ == '__main__':
    app.run(port=6666)
    # 对外接口测试
#     app.run('0.0.0.0', port=9135, debug=True)
