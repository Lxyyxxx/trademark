# requirements - tm
- elasticsearch
- gensim
- sklearn
- googletrans==4.0.0-rc1 (默认的 3.x 会报错)
- jieba
- eve
- bs4

# requirements - flask
- flask_wtf

# requirements - server
- js2py
