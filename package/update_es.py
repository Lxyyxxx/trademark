### 向 es 中添加新特征 ###

from elasticsearch import Elasticsearch

# from trademark.sim.SimHY import *
from trademark.sim.SimPY import *
# from trademark.sim.SimSJ import *
from trademark.sim.SimZX import *
# from trademark.sim.Server import *
from trademark.conf import *


# prehy = PreHY()
prepy = PrePY()
# presj = PreSJ()
prezx = PreZX()
# presv = PreServer()
es = Elasticsearch(ES_HOST, port=ES_PORT)

def replace_empty(tmcn):
    tmcn = tmcn.strip()
    return ''.join(tmcn)

def upd(item):
    tmcn = replace_empty(item['_source']['tmcn'])
    try:
        bs_group_id = prezx.getGroup(tmcn)
        py_group_id = prepy.getGroup(tmcn)
#         bs = prezx.getBishun(tmcn)
#         py = prepy.getPinyin(tmcn)
#         fc = prehy.getFenci(tmcn)
#         try:
#             en = prehy.getListEN(tmcn)
#         except Exception as e:
#             print(e)
#             en = None
#         code = presj.getCode(tmcn)
#         grp = presv.getGroupDict(item['_source'])
#         keys = presv.getKeys(grp)
        doc = item['_source']
        # id - 用于索引
        doc['bs_group_id'] = bs_group_id
        doc['py_group_id'] = py_group_id
#         # calculate
#         doc['bishun'] = bs
#         doc['pinyin'] = py
#         doc['hanyi_cn'] = fc
# #         doc['hanyi_en'] = en
#         doc['shijue'] = code
#         doc['fuwu'] = grp
#         doc['fuwu_keys'] = keys
#         print(tmcn, 'is finished!')
        es.index(index=item['_index'], id=item['_id'], body=doc)
    except KeyError:
        print(tmcn, 'is failed!')
        doc = item['_source']
        doc['valid_cn'] = False
    es.index(index=item['_index'], id=item['_id'], body=doc)
    del doc

def ubq(_id):
    qbody = {
        "slice": {
            "id": _id, 
            "max": 11
        },
        "query": {
            "bool": {
                "must": [
                    {"exists": {"field": "citycode"}}, # citycode 存在
                    {"regexp": {"tmcn": '[\u4e00-\u9fa5]+'}} # 中文
                ],
                "must_not": [
                    {"term": {"tmcn.keyword": "图形"}}, # 文字商标
                    {"exists": {"field": "py_group_id"}}, # 还没添加新特征
                    {"term": {"valid_cn": False}} # 不能是名字无效的商标
#                     {"regexp": {"tmcn": '[^\u4e00-\u9fa5]+'}} # 非中文
                ]
            }
        },
        'size': 10000
    }
    # first
    res = es.search(index=ES_ALL, body=qbody)#, scroll="3m"
    # update
    for hit in res['hits']['hits']:
        upd(hit)
    # after
    while res['hits']['hits']:
        res = es.scroll({'scroll': '3m', 'scroll_id': res['_scroll_id']})
        # update
        for hit in res['hits']['hits']:
            upd(hit)

if __name__ == '__main__':
    for i in range(1, 11):
        print(i, 'is running')
        ubq(i)
    print(es.search(index=ES_ALL, body={
        "query": {
            "bool": {
                "must": [
                    {"exists": {"field": "citycode"}}, # citycode 存在
                    {"regexp": {"tmcn": '[\u4e00-\u9fa5]+'}}, # 中文
                    {"exists": {"field": "py_group_id"}}, # 有新特征
                ],
                "must_not": [
                    {"term": {"tmcn.keyword": "图形"}}, # 文字商标
#                     {"regexp": {"tmcn": '[^\u4e00-\u9fa5]+'}} # 非中文
                ]
            }
        },
        'size': 1
    }))
