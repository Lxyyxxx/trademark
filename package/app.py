## Flask MAIN ##

from flask import Flask, render_template, request, redirect, session, send_file
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.widgets import TextInput, TextArea
from wtforms.validators import DataRequired, Regexp
import zipfile
import os

from api import EVE
from trademark.doc.Doc import *

app = Flask(__name__)

# 用于 post
app.secret_key = 'yxl'

# wtf 表单，用于 post 传参
class TMForm(FlaskForm):
    # CSS
    text_class = 'flex-1 appearance-none border border-transparent w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-md rounded-lg text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent'
    submit_class = 'w-full py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
    # 商标名
    tmcn = StringField(
        'tmcn',
        widget=TextInput(),
        render_kw={
            'class': text_class
        },
        # TODO 正则的错误如何提示出来？
        validators=[DataRequired(), Regexp(r'^[\u4e00-\u9fa5]+$', message='商标名必须为全中文！')]
    )
    # 异议人
    tmapplicant = StringField(
        'tmapplicant',
        widget=TextInput(),
        render_kw={
            'class': text_class,
        },
        validators=[DataRequired()]
    )
    # 品牌介绍
    tmintroduction = StringField(
        'tmintroduction',
        widget=TextArea(),
        render_kw={
            'class': text_class,
            'rows': '3'
        }
    )
    # 品牌影响力
    tminfluence = StringField(
        'tminfluence',
        widget=TextArea(),
        render_kw={
            'class': text_class,
            'rows': '3'
        }
    )
    # 提交
    tmsubmit = SubmitField(
        '提交',
        render_kw={
            'class': submit_class
        }
    )

class ChooseForm(FlaskForm):
    # CSS
    submit_class = 'w-full py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
    cssubmit = SubmitField(
        '下载',
        render_kw={
            'class': submit_class,
            'onClick': 'javascrip:setTimeout(function(){document.location = "/download";}, 1000)'
        }
    )


@app.route("/", methods=['GET', 'POST'])
def index():
    # get 显示表单
    if request.method == 'GET':
        form = TMForm()
        return render_template('index.html', form=form, title='首页 | 商标智能异议系统')
    # post 传表单的值
    elif request.method == 'POST':
        form = TMForm(request.form)
        # ✅
        if form.validate_on_submit():
            tmcn = request.form['tmcn']
            tmapplicant = request.form['tmapplicant']
            tmintroduction = request.form['tmintroduction']
            tminfluence = request.form['tminfluence']
            session['tmintroduction'] = tmintroduction
            session['tminfluence'] = tminfluence
            # API
            eve = EVE()
            js = eve.post({
                "tmcn": tmcn,
                "tmapplicant": tmapplicant
            })
            if js:
                _id = js['_id']
                tm = eve.get(_id)['tm2']
#                 session['tm'] = tm
                session['_id'] = _id
                choose = ChooseForm()
                return render_template('result.html', title='查询结果 | 商标智能异议系统', tmcn=tmcn, tmapplicant=tmapplicant, tmintroduction=tmintroduction, tminfluence=tminfluence, _id=_id, tm=tm, choose=choose)      
        # ❎
        return redirect('/error')

@app.route("/generate", methods=['GET', 'POST'])
def generate():
    # get 显示表单
    if request.method == 'GET':
        return redirect('/')
    # post 传表单的值
    elif request.method == 'POST':
        try:
            choose = ChooseForm(request.form)
            # 用户选择的值
            val = [int(i) for i in request.form.getlist('choose')]
            # 查询商标
            eve = EVE()
            _id = session['_id']
            tm = eve.get(_id)
            tm1 = tm['tm1']
            tmintroduction = session['tmintroduction']
            tminfluence = session['tminfluence']
            # 批量生成
            zip_name = '/tmp/%s.zip' % _id
            with zipfile.ZipFile(zip_name, 'w') as f:
                for i in val:
                    txt_name = DocTemplate(tm1, tm['tm2'][i], tmintroduction, tminfluence).to_txt()
                    f.write(txt_name)
                    os.remove(txt_name)
            return send_file(zip_name, as_attachment=True)
#             session['filename'] = zip_name
#             return redirect('/download/%s' % zip_name[5:])
    #         os.remove(zip_name) 
        except Exception:
            return redirect('/error')

@app.route("/download", methods=['GET'])
def download():
    return render_template('download.html', title='下载文档 | 商标智能异议系统')

@app.route("/error", methods=['GET'])
def error():
    return render_template('error.html', title='出错了 | 商标智能异议系统')

if __name__ == '__main__':
#     app.run(host='10.0.0.20', port=9010) # private
    app.run(host='0.0.0.0', port=9010) # public
