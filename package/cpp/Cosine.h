#include <cmath>
#include <vector>
/* 两个向量的乘积 */
double vecMul(std::vector<int> xfea, std::vector<int> yfea) {
    double res = 0.0;
    for (int i = 0; i < 4; i++) {
        res += (xfea[i] * yfea[i]);
    }
    return res;
}

/* 一个向量的模长 */
double vecMold(std::vector<int> fea) {
    double res = 0.0;
    for (int i : fea) {
        res += i * i;
    }
    return sqrt(res);
}

/* 余弦相似度 */
double CosineSimilarity(std::vector<int> xfea, std::vector<int> yfea) {
    double up = vecMul(xfea, yfea);
    double down = vecMold(xfea) * vecMold(yfea);
    return up / down;
}