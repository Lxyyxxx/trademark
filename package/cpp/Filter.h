// 滤波器
bool filter[4][2][2] = {
    // ㇀
    {{0, 1}, {1, 0}},
    // ㇔
    {{1, 0}, {0, 1}},
    // 一
    {{1, 1}, {0, 0}},
    // ㇑
    {{1, 0}, {1, 0}}
};
// 滤波器的个数及大小
int filter_num=4,filter_row=2,filter_col=2;
// 每个滤波器的和
int filter_add[4]={2,2,2,2};
// 局部滑动窗口的步长和窗口大小
int stride = 4;
int window_size = stride;
