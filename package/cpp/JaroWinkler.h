#include <algorithm>
#include <iostream>
#include <vector>

double JaroSimilarity(const std::string &a, const std::string &b) {
    int la = a.size(), lb = b.size();

    if (la == 0 || lb == 0) return 0.0;

    int mw = std::max(0, std::max(la, lb) / 2 - 1);
    std::vector<bool> fa(la, false);
    std::vector<bool> fb(lb, false);
    int m = 0;
    for (int i = 0; i < la; i++) {
        int min_index = std::max(i - mw, 0);
        int max_index = std::min(i + mw + 1, lb);
        for (int j = min_index; j < max_index; j++) {
            if (a[i] == b[j] && !fb[j]) {
                m++;
                fa[i] = true;
                fb[j] = true;
                break;
            }
        }
    }

    if (m == 0) return 0.0;

    int t = 0;
    std::vector<int> pa(m);
    std::vector<int> pb(m);
    for (int i = 0, ia = 0; i < la; i++) {
        if (fa[i]) pa[ia++] = i;
    }
    for (int j = 0, ib = 0; j < lb; j++) {
        if (fb[j]) pb[ib++] = j;
    }
    for (int i = 0; i < m; i++) {
        if (a[pa[i]] != b[pb[i]]) t++;
    }

    return (m * 1.0 / la + m * 1.0 / lb + (m - t * 1.0 / 2) * 1.0 / m) / 3.0;
}

double JaroWinklerSimilarity(const std::string &a, const std::string &b,
                             const double bt = 0.85, const double p = 0.1) {
    double sim_j = JaroSimilarity(a, b);

    if (sim_j <= bt) return sim_j;

    int la = a.size(), lb = b.size();

    int l = 0;
    int end_index = std::min(std::min(la, lb), 4);
    for (int i = 0; i < end_index; i++)
        if (a[i] == b[i])
            l++;
        else
            break;

    return sim_j + l * p * (1 - sim_j);
}
