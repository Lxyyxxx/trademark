#include <pthread.h>
#include <unistd.h>
#include "Similarity.h"

// 用于格式化字符串
const int SIZE = 100;
char buf[SIZE];
// 线程个数
const int NUMS = 10;

/* 字符串格式化 */
std::string format(int d) {
    snprintf(buf, SIZE, "../hansxy/xy%05d", d);
    return std::string(buf);
}

/* 运行 */
void *run(void *argv) {
    int num = *(int *)argv;
    printf("num=%d pid=%d is running ...\n", num, getpid());
    std::string filename = format(num);
    readcal(filename);
    printf("num=%d pid=%d is finished!\n", num, getpid());
}

int main() {
    pthread_t tids[NUMS];
    int current[NUMS];
    for (int i = 0; i < 21844; i += NUMS) {
        for (int j = 0; j < NUMS; j++) {
            current[j] = i + j;
            pthread_create(&tids[j], NULL, run, (void *)&current[j]);
        }
        for (int j = 0; j < NUMS; j++) {
            pthread_join(tids[j], NULL);
        }
    }
    return 0;
}
/*

mkdir build
time g++ --std=c++17 -o build/main.o main.cpp
-I/usr/local/include/mongocxx/v_noabi -I/usr/local/include/bsoncxx/v_noabi
-L/usr/local/lib -lmongocxx -lbsoncxx -pthread
time ./build/main.o

time=15h
===
batch   00010-19789
real    777m40.500s
user    3506m23.063s
sys     6m42.250s
---
batch   19790-21673
real    107m36.647s
user    326m26.500s
sys     0m51.031s
---
batch   21674-21841
real    8m33.628s
user    28m23.813s
sys     0m3.891s
---
batch   00000-00009
real    0m23.678s
user    1m43.984s
sys     0m0.406s
---
batch   21842-21843
real    0m8.189s
user    0m11.188s
sys     0m0.109s
*/