#include <string.h>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <fstream>
#include <iostream>
#include <memory>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <numeric>

#include "Cosine.h"
#include "Filter.h"
#include "JaroWinkler.h"
#include "Lattice.h"
#include "pinyin.h"
#include "stroke.h"

// 第一个汉字的unicode
const int first_word_unicode = 19968;
// 相似阈值
const double py_threshold = 0.895;  // 0.90
const double zx_threshold = 0.945;  // 0.95
const double dz_threshold = 0.945;  // 0.95
// MongoDB实例
mongocxx::instance inst{};

/* 相当于Python3中的chr */
std::string chr(uint16_t unicode) {
    char i1 = 224 | (unicode >> 12), i2 = 128 | ((unicode >> 6) & 63),
         i3 = 128 | (unicode & 63);
    std::string utf8{i1, i2, i3};
    return utf8;
}

/* 相当于Python3中的ord */
uint16_t ord(std::string utf8) {
    int b1 = utf8[0] & 15, b2 = (utf8[1] & 60) >> 2,
        b3 = ((utf8[1] & 3) << 2) ^ ((utf8[2] & 48) >> 4), b4 = utf8[2] & 15;
    return b1 * 4096 + b2 * 256 + b3 * 16 + b4;
}

/* 单个中文的unicode十六进制转拼音编码 */
std::vector<std::string> word2pinyin(uint16_t unicode) {
    std::vector<std::string> pylist;
    char *p = strtok(pinyin[unicode - first_word_unicode].data(), " ");
    while (p != NULL) {
        pylist.push_back(p);
        p = strtok(NULL, " ");
    }
    return pylist;
}

/* 两个汉字的拼音相似度 */
double simPinyin(uint16_t uniX, uint16_t uniY) {
    std::vector<std::string> xpy, ypy;
    double Max = 0.0;
    xpy = word2pinyin(uniX);
    ypy = word2pinyin(uniY);
    for (std::string x : xpy) {
        for (std::string y : ypy) {
            Max = std::max(Max, JaroWinklerSimilarity(x, y));
        }
    }
    return Max;
}

/* 单个中文的unicode十六进制转笔顺编码 */
std::string word2stroke(uint16_t unicode) {
    return stroke[unicode - first_word_unicode];
}

/* 两个汉字的笔顺相似度 */
double simStroke(uint16_t uniX, uint16_t uniY) {
    std::string xstk, ystk;
    xstk = word2stroke(uniX);
    ystk = word2stroke(uniY);
    return JaroWinklerSimilarity(xstk, ystk);
}

/* 单个中文的unicode十六进制转点阵 */
bool (*word2lattice(uint16_t unicode))[16] {
    return lattice[unicode - first_word_unicode];
}

/* 边缘填充一圈0 */
std::vector<std::vector<bool>> padLattice(uint16_t unicode) {
    bool(*lat)[16];
    lat = word2lattice(unicode);
    std::vector<std::vector<bool>> padlat(18, std::vector<bool>(18, 0));
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            padlat[i + 1][j + 1] = lat[i][j];
        }
    }
    return padlat;
}

/* 局部矩阵做滑动窗口相乘滤波，得到一个特征向量 */
std::vector<int> convolution(std::vector<std::vector<bool>> padlat) {
    std::vector<int> res(filter_num, 0);
    int filter_sum;
    // 遍历滑动窗口
    for (int pi = 0; pi < padlat.size() - 1; pi++) {
        for (int pj = 0; pj < padlat[0].size() - 1; pj++) {
            // 遍历滤波器
            for (int fn = 0; fn < filter_num; fn++) {
                filter_sum = 0;
                for (int fi = 0; fi < filter_row; fi++) {
                    for (int fj = 0; fj < filter_col; fj++) {
                        if (padlat[pi + fi][pj + fj] & filter[fn][fi][fj])
                            filter_sum++;
                    }
                }
                res[fn] += (filter_sum / 2);
            }
        }
    }
    return res;
}

/* 两个汉字的点阵相似度 */
double simLattice(uint16_t uniX, uint16_t uniY) {
    std::vector<std::vector<bool>> xlat = padLattice(uniX),
                                   ylat = padLattice(uniY);
    int num_local_matrix = 16, cur, sumx, sumy;
    // 初始化相似结果为0
    std::vector<double> sim_all(num_local_matrix, 0);
    for (int i = 0; i < 16; i += stride) {
        for (int j = 0; j < 16; j += stride) {
            std::vector<std::vector<bool>> xlatloc(6, std::vector<bool>(6)),
                ylatloc(6, std::vector<bool>(6));
            for (int li = 0; li < 6; li++) {
                for (int lj = 0; lj < 6; lj++) {
                    xlatloc[li][lj] = xlat[i + li][i + lj];
                    ylatloc[li][lj] = ylat[i + li][i + lj];
                }
            }
            std::vector<int> xfeature = convolution(xlatloc),
                             yfeature = convolution(ylatloc);
            cur = i / stride * 4 + j / stride;
            sumx = std::accumulate(xfeature.begin(), xfeature.end(), 0);
            sumy = std::accumulate(yfeature.begin(), yfeature.end(), 0);
            /* 数据库里原来跑的
            if (sumx == 0 && sumy == 0) {
                sim_all[cur] = 1.0;
            } else if (sumx == 0) {
                sim_all[cur] = (1.0 - sumy / 200.0);
            } else if (sumy == 0) {
                sim_all[cur] = (1.0 - sumx / 200.0);
            } else {
                sim_all[cur] = CosineSimilarity(xfeature, yfeature);
            }
            */
            if (sumx == 0 || sumy == 0) {
                double sumxy = 0;
                for (int li = 0; li < 6; li++) {
                    for (int lj = 0; lj < 6; lj++) {
                        sumxy += int(xlat[i + li][i + lj] ^ ylat[i + li][i + lj]);
                        // std::cout << sumxy << std::endl;
                    }
                }
                sim_all[cur] = (1 - sumxy / 36);
            } else {
                sim_all[cur] = CosineSimilarity(xfeature, yfeature);
            }
        }
    }
    double sum = std::accumulate(sim_all.begin(), sim_all.end(), 0.0);
    return sum / num_local_matrix;
}

/* 打印矩阵 */
void printMatrix(bool (*x)[16]) {
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            std::cout << x[i][j] << " ";
        }
        std::cout << '\n';
    }
}
void printMatrixPad(std::vector<std::vector<bool>> x) {
    for (int i = 0; i < 18; i++) {
        for (int j = 0; j < 18; j++) {
            std::cout << x[i][j] << " ";
        }
        std::cout << '\n';
    }
}

/* 读取csv并计算相似度 */
void readcal(std::string inputfile) {
    std::cout << inputfile << " is running..\n";
    std::string s, x, y;
    uint16_t xid, yid;
    double pysim, zxsim, dzsim, restype;
    bool pyres, zxres, dzres;
    std::ifstream in(inputfile, std::ios::in);
    std::vector<bsoncxx::document::value> documents;
    mongocxx::client conn{mongocxx::uri{"mongodb://YOUR_IP:YOUR_PORT/"}};
    auto collection = conn["YOUR_DATABASE"]["YOUR_COLLECTION"];
    while (getline(in, s)) {
        x = s.substr(0, 3);
        y = s.substr(4, 3);
        // std::cout << x << " " << y << '\n';
        xid = ord(x);
        yid = ord(y);
        // std::cout << xid << " " << yid << '\n';
        // 拼音
        pysim = simPinyin(xid, yid);
        pyres = (pysim >= py_threshold);
        // 笔顺
        zxsim = simStroke(xid, yid);
        zxres = (zxsim >= zx_threshold);
        // 点阵
        dzsim = simLattice(xid, yid);
        dzres = (dzsim >= dz_threshold);
        //  结果类型，仅为笔顺和点阵的状态集合
        restype = (zxres << 1) ^ dzres;
        documents.push_back(bsoncxx::builder::stream::document{}
                            << "word_x" << x << "word_y" << y << "pinyin_sim"
                            << pysim << "pinyin_res" << pyres << "zixing_sim"
                            << zxsim << "zixing_res" << zxres << "dianzhen_sim"
                            << dzsim << "dianzhen_res" << dzres << "res_type"
                            << restype << bsoncxx::builder::stream::finalize);
        /*std::cout << "pinyin:" << simPinyin(xid, yid) << '\n';
        std::cout << "stroke:" << simStroke(xid, yid) << '\n';
        std::cout << "lattice:" << simLattice(xid, yid) << '\n';*/
    }
    // mtx.lock();
    collection.insert_many(documents);
    // mtx.unlock();
    in.close();
    std::cout << inputfile << " is finished..\n";
}