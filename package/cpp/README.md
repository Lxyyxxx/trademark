# Similarity C++
使用C++编写了一个计算拼音、字形、点阵相似度的程序  
目前只支持比较单个汉字  
**Environment:**
```bash
Windows 10 Linux Subsystem
BUILD:    18362
BRANCH:   19h1_release
RELEASE:  Ubuntu 18.04.4 LTS
KERNEL:   Linux 4.4.0-18362-Microsoft
```

## 安装
### Step 1 Cmake
```bash
sudo apt install cmake
```

### Step 2 mongocxx driver
[installation](http://mongocxx.org/mongocxx-v3/installation/)  
First. MongoDB C driver   
Second. mongocxx driver

## 使用
### Step 1 Modify
将`Similarty.h`中的`readcal()`里的
```cpp
mongocxx::client conn{mongocxx::uri{"mongodb://YOUR_IP:YOUR_PORT/"}};
auto collection = conn["YOUR_DATABASE"]["YOUR_COLLECTION"];
```
修改为自己想要的

将`main.cpp`中的`NUMS`修改为自己想要的进程数  
对比文件默认路径为`../hansxy/xy%05d`，形式为`X,Y`，总文件个数为`21844`，在`main()`中第一个`for`循环（其中每个文件测试数据为10000个）

### Step 2 Compile
```bash
mkdir build
time g++ --std=c++17 -o build/main.o main.cpp
-I/usr/local/include/mongocxx/v_noabi -I/usr/local/include/bsoncxx/v_noabi
-L/usr/local/lib -lmongocxx -lbsoncxx -pthread
```

### Step 3 Run
```bash
time ./build/main.o
```