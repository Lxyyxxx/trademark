## 按需查找的 ES API ##

from elasticsearch import Elasticsearch
from trademark.conf import *
es = Elasticsearch(ES_HOST, port=ES_PORT)

qbody = {
    "query": {
        "constant_score" :{
            "filter": {
                "bool": {
                    "must": [
                        # fill
                    ]
                }
            }
        }
    },
    "size": 10000,
#     "_source": ['citycode', 'intcls', 'tmdetail', 'tmagent', 'regno', 'appdate', 'py_group_id', 'bs_group_id', 'uid', 'tmcn', 'tmapplicant', 'tmstatus']
}

def by_regno(regno):
    qbody['query']['constant_score']['filter']['bool']['must'] = [{"term": {"regno": regno}}]
    return es.search(index=ES_ALL, body=qbody)

def by_tmcn(tmcn):
    qbody['query']['constant_score']['filter']['bool']['must'] = [{"term": {"tmcn.keyword": tmcn}}]
    return es.search(index=ES_ALL, body=qbody)

def by_tmapplicant(tmapplicant):
    qbody['query']['constant_score']['filter']['bool']['must'] = [{"term": {"tmapplicant.keyword": tmapplicant}}]
    return es.search(index=ES_ALL, body=qbody)

def by_tmapplicant_and_tmcn(tmapplicant, tmcn):
    qbody['query']['constant_score']['filter']['bool']['must'] = [
        {"term": {"tmapplicant.keyword": tmapplicant}}, 
        {"term": {"tmcn.keyword": tmcn}}, 
    ]
    return es.search(index=ES_ALL, body=qbody)

def by_intcls_and_tmcn(intcls, tmcn):
    qbody['query']['constant_score']['filter']['bool']['must'] = [
        {"term": {"intcls": intcls}}, 
        {"term": {"tmcn.keyword": tmcn}}, 
    ]
    return es.search(index=ES_ALL, body=qbody)