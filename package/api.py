## EVE 有关的 API ##

import requests
import json

# 给 eve 发数据，并根据 _id 查询结果
class EVE:
    def __init__(self):
        self.post_url = 'http://127.0.0.1:6666/trademarks'
        self.post_headers = {'Content-Type': 'application/json'}
        self.get_url = 'http://127.0.0.1:6666/trademarks/'
    
    """
    data = {
        'tmcn': 商标（仅含中文）,
        'tmapplicant': 异议人
    }
    """
    def post(self, data):
        try:
            res = requests.post(self.post_url, headers=self.post_headers, data=json.dumps(data))
            return json.loads(res.text)
        except Exception:
            return None
    
    def get(self, _id):
        self.get_url += _id
        res = requests.get(self.get_url)
        return json.loads(res.text)

