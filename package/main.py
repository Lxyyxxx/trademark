### 命令行形式的系统 ###
"""
2021年1月22日
CPU times: user 24.8 ms, sys: 16.3 ms, total: 41.1 ms
Wall time: 2min 20s
"""

import requests
import json
import re

from trademark.doc.Doc import *

# 给 eve 发数据，并根据 _id 查询结果
class EVE:
    def __init__(self):
        self.post_url = 'http://127.0.0.1:6666/trademarks'
        self.post_headers = {'Content-Type': 'application/json'}
        self.get_url = 'http://127.0.0.1:6666/trademarks/'
    
    """
    data = {
        'tmcn': 商标（仅含中文）,
        'tmapplicant': 异议人
    }
    """
    def post(self, data):
        try:
            res = requests.post(self.post_url, headers=self.post_headers, data=json.dumps(data))
            return json.loads(res.text)
        except Exception:
            return None
    
    def get(self, _id):
        self.get_url += _id
        res = requests.get(self.get_url)
        return json.loads(res.text)

# 询问用户 + 生成异议文档
class ASK:
    def __init__(self, res):
        self.res = res
        self.tms = []
    
    def show(self):
        # tm1 信息
        # tm2 及 结果
        tm2 = self.res['tm2']
        for i, v in enumerate(tm2):
            print('编号：%d' % i)
            print('- 商标信息 -')
            print('商标名：%s' % v['tmcn'])
            print('申请人：%s' % v['tmapplicant'])
            print('类别：%s' % v['intcls'])
            print('服务：%s' % '、'.join(v['tmdetail']))
            print('注册号：%s' % v['regno'])
            print('城市编码：%s' % v['citycode'])
            print('状态：%s' % v['tmstatus'])
            print('- 检测结果 - ')
            print('拼音是否相似：%s' % v['res']['pinyin']['res'])
            print('字形是否相似：%s' % v['res']['zixing']['res'])
            print('含义是否相似：%s' % v['res']['hanyi']['res'])
            print('视觉是否相似：%s' % v['res']['shijue']['res'])
            if v['res']['server']['same']['code']:
                print('所有同类服务：%s' % '、'.join(v['res']['server']['same']['code']))
            else:
                print('所有同类服务：无')
            if v['res']['server']['most_diff']['uid']:
                print('最相似的服务：%s 和 %s' % (v['res']['server']['most_diff']['tm1'], v['res']['server']['most_diff']['tm2']))
            else:
                print('最相似的服务：无')
            if v['res']['server']['most_same']['code']:
                print('最相同的服务编码：%s' % v['res']['server']['most_same']['code'])
            else:
                print('最相同的服务编码：无')
            if v['res']['aim']:
                print('有恶意申请商标的行为')
                for i in v['res']['aim']:
                    print('在 %d 年，共申请了 %d 个商标，分别为：%s' % (i['year'], i['cnt'], i['tm']))
            else:
                print('没有恶意申请商标的行为')
            print('是否在同一城市：%s' % v['res']['locate'])
            print('#############################################')
    
    def choose(self):
        cs = input('输入需要生成异议文档的商标编号：')
        # 提取数字
        nums = re.findall('\d+', cs)
        for i in nums:
            self.tms.append(self.res['tm2'][int(i)])
    
    def getDoc(self):
        for tm2 in self.tms:
            DocTemplate(self.res['tm1'], tm2).to_txt()
        print('已成功生成异议文档！')
    
    def run(self):
        self.show()
        self.choose()
        self.getDoc()

if __name__ == '__main__':
    eve = EVE()
#     js = eve.post({
#         "tmcn": "新安", 
#         "tmapplicant": "浙江新安化工集团股份有限公司"
#     })
    tmcn = input("请输入异议商标名（仅限中文）：")
    tmapplicant = input("请输入异议人：")
    js = eve.post({
        "tmcn": tmcn, # "柏莱曼", 
        "tmapplicant": tmapplicant# "浙江欧巴赫集成科技有限公司"
    })
    if js:
        print(js['_id'])
        ask = ASK(eve.get(js['_id']))
        ask.run()
    else:
        print('未查询到异议人相关信息！')