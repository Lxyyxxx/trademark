import time

import requests 
from bs4 import BeautifulSoup 

import json
import js2py

class spyder(object):

    def __init__(self,url):
        self.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'}
        self.base_url = url

    def get(self):
        content = requests.get(self.base_url, headers=self.headers)
        return content.text

    def get_and_soup(self,conde_format= 'utf-8'):
        try:
            content = requests.get(self.base_url, headers=self.headers)
        except Exception as e:
            print(e)
            try:
                content = requests.get(self.base_url, headers=self.headers)
            except:
                pass
        content.encoding = conde_format
        #print(content.text)
        soup = BeautifulSoup(content.text,'html.parser')
        #print(soup)
        return soup

    def get_and_json(self):
        content = requests.get(self.base_url,headers = self.headers)
        content = content.text
        js = js2py.eval_js(content)
        return js


if __name__=='__main__':
    sp = spyder('http://www.njchangkeip.com/NiceClassification/NiceClassification/219')
    print(sp.get_and_soup())

