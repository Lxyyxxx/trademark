# -*- coding: utf-8 -*-
#《类似商品和服务区分表》爬取与处理

from ReingisticSpyder import spyder
from opmongo import Mongo
from string import digits
import re

class GetGroup(object):
    """获取《类似商品和服务区分表》中各个组的名称与代码，存入mongodb中的group中 """
    def __init__(self):
        #215-259
        self.base_url = 'http://www.njchangkeip.com/NiceClassification/NiceClassification/'
        #存放所需要的所有页面内容，all_text还可以做更多的抽取，但这里暂时不需要
        self.all_text = []
        #所有的class【组别】,如：• 0101 工业气体，单质
        self.all_group = []

    #用来去掉空的list
    def panduan(self,s):
        if s==[]:
            return None
        else:
            return s
    
    #获取所有group，并进行处理，最终存入mongodb
    def get_all_group(self):
        for i in range(215,260):
            url = self.base_url+str(i)
            o  = spyder(url).get_and_soup().find_all(class_="MsoNormal")
            self.all_text = self.all_text+ [k.get_text().split('新增：')[0].split('注：')[0] for k in o]
        self.all_group = self.all_group + list(filter(self.panduan,[re.findall('•.+',k) for k in self.all_text]))
            
        #存放的表
        cl = Mongo('group')
        for i in self.all_group:
            li = (i[0].split(' '))
            if len(li) == 2:
                li.append(li[1][4:])
                li.append(li[1][:4])
                li.pop(1)
            if len(li)<3:
                li =(i[0].split('\xa0'))

                
            item = {'group':li[1],
                    'gName':li[2]
                    }
            cl.insert_one(item)
    

class GetGroupDetail(object):
    """获取每个group中具体包含了啥内容"""
    def __init__(self):
        #取出所有group的代码
        cl = Mongo('group')
        self.all_group = []
        for i in cl.get_all_data():
            self.all_group.append(i['group'])

    def get(self):   
        #爬每个group的part[part分析过分困难，暂时先为content]
        partcl =Mongo('group_detail')
        base_url = 'http://sbfl.cn/show.asp?bh='

        for i in self.all_group:
            url = base_url+str(i)
            try:
                content = spyder(url).get_and_soup(conde_format='gbk').find(class_="sbflnr").get_text()
            except:
                continue
            item = {'class':i,
                    'content': content
                    }
            partcl.insert_one(item)

class RegulateGroup(object):
    """格式化各个group中的content，方便比较"""
    def __init__(self):
        pass
        
    #去除字符串中的数字与C
    def del_digit(self,s):
        remove_digits = str.maketrans('', '', digits)
        res = s.translate(remove_digits)
        res = res.replace('C','').strip()
        return res

    #去除字符串中的数字、C、括号
    def del_more(self,s):
        remove_digits = str.maketrans('', '', digits)
        res = s.translate(remove_digits)
        res = res.replace('C','').strip()
        res = re.sub('（.+）','',res)
        return res

    #仅仅只去掉数字与c，效果不如去掉括号来的好
    def regular_rough(self):
        item = {}
        sourse = Mongo('group_detail')
        aim =Mongo('rgroup')
        for i in sourse.get_all_data():
            item['group'] = i['class']
            item['content']  = list(map(self.del_digit,i['content'].split('；')))
            aim.insert_one(item)
            item = {}
    
    #更为细致的规格化，去掉数字、c、括号
    def regular_careful(self):
        item = {}
        sourse = Mongo('group_detail')
        aim =Mongo('group_item')
        for i in sourse.get_all_data():
            item['group'] = i['class']
            item['content']  = list(map(self.del_more,i['content'].split('；')))
            aim.insert_one(item)
            item = {}

    #将信息整合为一个集合
    def assembly_info(self):
        infor1 = []
        [infor1.append(i) for i in Mongo('group_detail').get_all_data()]
        infor2 = []
        [infor2.append(i) for i in Mongo('group_item').get_all_data()]
        infor3 = []
        [infor3.append(i) for i in Mongo('group').get_all_data()]


if __name__=='__main__':
    # group
    GetGroup().get_all_group()
    # group detail
    GetGroupDetail().get()
    # group item
    RegulateGroup().regular_careful()
    # group info
    infor1 = []
    [infor1.append(i) for i in Mongo('group_detail').get_all_data()]
    infor2 = []
    [infor2.append(i) for i in Mongo('group_item').get_all_data()]
    infor3 = []
    [infor3.append(i) for i in Mongo('group').get_all_data()]

    for i in infor3:
        item = {'groupNumber':i['group'],
                'groupName': i['gName'],
                'content':'',
                'product':[]}
        for j in infor1:
            if item['groupNumber'] == j['class']:
                item['content'] = j['content']
        for k in infor2:
            if item['groupNumber'] == k ['group']:
                item['product'] = k['content']
        #print(item)
        Mongo('group_info').insert_one(item)
