import  pymongo

class Mongo():

    def __init__(self, collection=None):
        self.myclient = pymongo.MongoClient("mongodb://shangbiaoroot:China%402020!@10.0.0.9/shangbiao?authSource=admin")
        self.mydb = self.myclient["shangbiao"]
        if collection:
            self.mycol = self.mydb[collection]

    # item = {'_id': i['_id']}
    def insert_one(self,item):
        if '_id' in item:
            del item['_id']
        self.mycol.insert_one(item)
#         print(item)
    
    #big data process use no_cursor_timeout = True
    def get_all_data(self):
        data = self.mycol.find()
        return data

    # condition = {'_id': i['_id']}
    # update = {'zwxx_split': lists}
    def update_one_mongo(self, condition, update):
        count = 0
        try:
            self.mycol.update_one(condition, {'$set': update})
            #if ((count % 300) == 0):
            #    print(count)
            print('update success')
        except Exception as e:
            print(e)
        print(update)

    # 清空所有内容
    def remove(self):
        self.mycol.remove()
        
    # 删库
    def drop(self):
        self.mycol.drop()
        
    def close(self):
        self.myclient.close()



if __name__=="__main__":
#     mongo = Mongo('relationQDII ')
#     a = mongo.get_all_data()
#     for i in a:
#         print(i)
    Mongo()