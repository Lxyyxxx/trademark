### eve settings ###

from trademark.conf import MONGO_PORT, MONGO_SB, MONGO_HOST, MONGO_AUTH_SOURCE, MONGO_PASSWD, MONGO_USERNAME

MONGO_HOST = MONGO_HOST
MONGO_PORT = MONGO_PORT
MONGO_DBNAME = MONGO_SB
# Skip this block if your db has no auth. But it really should.
MONGO_USERNAME = MONGO_USERNAME
MONGO_PASSWORD = MONGO_PASSWD
# Name of the database on which the user can be authenticated,
# needed if --auth mode is enabled.
MONGO_AUTH_SOURCE = 'admin'

# MONGO_DBNAME = 'apitest'

RESOURCE_METHODS = ['GET', 'POST']
ITEM_METHODS = ['GET', 'PATCH', 'PUT']

# 需要的字段
schema = {
    # request
    # 异议人商标（中文）
    'tmcn': {
        'type': 'string',
        'required': True
    },
    # 异议人
    'tmapplicant': {
        'type': 'string',
        'required': True
    },
#     # 类别
#     'intcls': {
#         'type': 'string',
#         'allowed': ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26',
#  '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45']
#     },
#     # 注册号
#     'regno': {
#         'type': 'string',
#         'unique': True,
#     },
    # result
    # 异议人商标
    'tm1': {
        'type': 'dict'
    },
    # 被异议人商标
    'tm2': {
        'type': 'list'
    }
}

# 查询 url 配置
trademarks = {
    'item_title': 'trademark',
    'resource_methods': ['GET', 'POST'],
    'ITEM_METHODS' : ['PUT','POST'],
    'schema': schema
}

DOMAIN = {'trademarks': trademarks}
